using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public int health;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Z)) fire();

        if (Input.GetKeyDown(KeyCode.T)) ring();

        transform.position += new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * speed * Time.deltaTime;
    }

    private void fire()
    {
        GameObject bul = BulletPool.SharedInstance.GetPooledBullet("AlphaBullet");

        bul.transform.position = transform.position;
        bul.transform.rotation = transform.rotation;
        bul.SetActive(true);
    }

    private void ring()
    {
        GameObject bul = BulletPool.SharedInstance.GetPooledBullet("AlphaBullet");

        bul.transform.position = transform.position;
        bul.transform.rotation = transform.rotation;
        bul.SetActive(true);
        var test = bul.GetComponent<bullet>();
        test.speed = 7;
    }
}
