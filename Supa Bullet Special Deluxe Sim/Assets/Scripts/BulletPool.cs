using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{

    [System.Serializable]
    public class BulletPoolObject
    {
        public GameObject bulletToPool;
        public int amountToPool;
        public bool BulletOverflow = true;
    }

    public static BulletPool SharedInstance;
    public List<BulletPoolObject> typeToPool;
    public List<GameObject> pooledBullets;
    

    void Awake()
    {
        SharedInstance = this;
    }

    void Start()
    {
        pooledBullets = new List<GameObject>();
        foreach (BulletPoolObject bullet in typeToPool)
        {
            for (int i =0; i < bullet.amountToPool; i++)
            {
                GameObject bul = (GameObject)Instantiate(bullet.bulletToPool);
                bul.SetActive(false);
                pooledBullets.Add(bul);
            }
        }
    }

    public GameObject GetPooledBullet(string tag)
    {
        for (int i = 0; i < pooledBullets.Count; i++)
        {
            if (!pooledBullets[i].activeInHierarchy && pooledBullets[i].tag == tag)
            {
                return pooledBullets[i];
            }
        }
        foreach (BulletPoolObject bullet in typeToPool)
        {
            if (bullet.bulletToPool.tag == tag)
            {
                if (bullet.BulletOverflow)
                {
                    GameObject bul = (GameObject)Instantiate(bullet.bulletToPool);
                    bul.SetActive(false);
                    pooledBullets.Add(bul);
                    return bul;
                }
            }
        }

        return null;
    }
}
