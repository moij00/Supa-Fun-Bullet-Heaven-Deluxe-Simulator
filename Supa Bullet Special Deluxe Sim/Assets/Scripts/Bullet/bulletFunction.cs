using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletFunction : MonoBehaviour
{

    // change bullet
    public int amount;
    public float speed;
    public float maxSpeed;
    public float acceleration;
    public float angle;
    public float minAngle;
    public float maxAngle;
    public float delay;

    // change emittor 
    public bool emitRotate;
    public float emitMinAngle;
    public float emitMaxAngle;

    // TODO: Create premade bullet Object to fire as bullet prefab as well! 

    public static bulletFunction bulletType;

    void Awake()
    {
        bulletType = this;
    }

    void Ring(int amount, float speed, float maxSpeed, float acceleration, float delay)
    {
        return;
    }

    void Spread()
    {
        return;
    }

    void Stack()
    {
        return;
    }

     
}
