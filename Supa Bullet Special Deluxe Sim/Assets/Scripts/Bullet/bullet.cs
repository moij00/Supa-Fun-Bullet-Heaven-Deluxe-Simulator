using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{

    public Vector2 velocity;
    public float speed;
    //public float accel;
    //public float maxSpeed;
    //public float delay;
    public float lifeTime;

    private void OnEnable()
    {
        Invoke("Destroy", lifeTime);
    }

    void Update()
    {
        transform.Translate(speed * Time.deltaTime * velocity);
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
}
